import React from "react";
import ReactDOM from "react-dom";

import { createStore } from "redux";
import { connect, Provider } from "react-redux";
import reducer from "./reducers";

import Header from "./components/Header.js";
import ProductBody from "./components/ProductBody.js";

const store = createStore(reducer);

const App = () => {
  return (
    <div>
      <Header />
      <ProductBody />
    </div>
  );
};

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
