import React from "react";
import * as d3 from "d3";

export default class LineChart extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    this.drawAxis();
  }

  drawAxis() {
    if (!this.props.timeseries) return;
    const xScale = d3
      .scaleTime()
      .domain(d3.extent(this.props.timeseries, (d) => d.weekEnding))
      .range([30, this.props.width]);

    const xAxis = d3
      .axisBottom(xScale)
      .tickSizeOuter(0)
      .tickFormat(d3.timeFormat("%b"));

    d3.select("svg#line-chart")
      .append("g")
      .attr("transform", `translate(0,300)`)
      .call(xAxis);
  }

  render() {
    const timeseries = this.props.timeseries;
    if (!timeseries) return <div>Chart data not ready</div>;
    const width = this.props.width;
    const height = 300;
    const margin = 30;

    // We have to convert the weekEnding values to Dates
    for (let week in timeseries) {
      timeseries[week].weekEnding = new Date(timeseries[week].weekEnding);
    }

    timeseries.sort((left, right) => left < right);

    const xScale = d3
      .scaleTime()
      .domain(d3.extent(timeseries, (d) => d.weekEnding))
      .range([margin, width]);

    const yScale = d3
      .scaleLinear()
      .domain(d3.extent(timeseries, (d) => d.retailSales))
      .range([height, margin]);

    const line = d3
      .line()
      .x((d) => xScale(d.weekEnding))
      .y((d) => yScale(d.retailSales))
      .curve(d3.curveBasis);

    // TODO: Should really make yScale take in wholesaleSales values so
    // nothing becomes out of bounds.
    const lazyLine = d3
      .line()
      .x((d) => xScale(d.weekEnding))
      .y((d) => yScale(d.wholesaleSales))
      .curve(d3.curveBasis);

    return (
      <div>
        <h2>Retail Sales</h2>
        <svg id="line-chart" height={height + margin} width={width + margin}>
          <path id="gray" d={lazyLine(timeseries)} />
          <path d={line(timeseries)} />
        </svg>
      </div>
    );
  }
}
