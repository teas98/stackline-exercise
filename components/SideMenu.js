import React from "react";

import house from "../assets/house.png";
import bars from "../assets/bars.png";

const SideMenu = ({ image, title, subtitle, tags }) => {
  const tagElements = [];
  for (let tag in tags) {
    tagElements.push(
      <div className="tag" key={tag}>
        {tags[tag]}
      </div>
    );
  }

  return (
    <div className="side-menu">
      <img src={image} />
      <h2>{title}</h2>
      <p>{subtitle}</p>
      <div className="tag-container">{tagElements}</div>
      <div className="menu-link">
        <img src={house} />
        OVERVIEW
      </div>
      <div className="menu-link">
        <img src={bars} />
        SALES
      </div>
    </div>
  );
};

export default SideMenu;
