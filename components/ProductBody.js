import React from "react";
import { connect } from "react-redux";
import { setProductData } from "../actions/index.js";
import SideMenu from "./SideMenu.js";
import LineChart from "./LineChart.js";

import projectData from "../assets/demo_data.json";

class ProductBody extends React.Component {
  constructor(props) {
    super(props);
    props.setProductData(projectData[0]);
  }

  render() {
    const pd = this.props.productData;
    const width = Math.floor(window.innerWidth * 0.65);
    return (
      <div className="product-body">
        <SideMenu
          image={pd.image}
          title={pd.title}
          subtitle={pd.subtitle}
          tags={pd.tags}
        />
        <div className="chart-container">
          <LineChart timeseries={pd.sales} width={width}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  productData: state.productData,
});

export default connect(mapStateToProps, { setProductData })(ProductBody);
