import React from "react";
import stacklineImage from "../assets/stackline.png";

const Header = () => {
  return (
    <div className="header">
      <img src={stacklineImage} />
    </div>
  );
};

export default Header;
