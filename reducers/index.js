import { SET_PRODUCT_DATA } from "../actions/index.js";

const initialState = {
  productData: {},
};

export default function reducer(state = initialState, action) {
  if (action.type === SET_PRODUCT_DATA) {
    state.productData = action.data;
    return { ...state };
  } else {
    return state;
  }
}
