export const SET_PRODUCT_DATA = "SET_PRODUCT_DATA";

export const setProductData = (data) => ({
  type: SET_PRODUCT_DATA,
  data,
});
